import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TooltipModule, DropdownModule, ConfirmationService} from 'primeng/primeng';

import { HeaderModule } from './framework/header/header.module';
import { FooterModule } from './framework/footer/footer.module';
import { ErrorsModule } from './framework/errors/errors.module';

import {
    InputTextModule, ButtonModule, DataTableModule, DialogModule,
    GrowlModule, AccordionModule, CalendarModule, InputMaskModule,
    FieldsetModule, DataListModule, ConfirmDialogModule
    } from 'primeng/primeng';
import { EnterKeyModule } from './framework/enter-key/enter-key.module';
import { CustomFormModule } from './framework/custom-form/custom-form.module';

import { TerminalClientService } from './framework/services/terminal-client.service';
import { AuthService } from './framework/services/auth.service';
import { MessagesService } from './framework/services/messages.service';
import { SignalDirective } from './framework/directives/signal.directive';
import { NgLoggerModule, Level } from '@nsalaun/ng-logger';
import { FunKeyDirective } from './framework/directives/fun-key.directive';
import { FunctionKeyPanelComponent } from './framework/function-key-panel/function-key-panel.component';
import { BaseViewComponent } from './framework/base-view/base-view.component';
import { AutoFocusDirective } from './framework/directives/auto-focus';
import { CapitalizeDirective } from './framework/directives/capitalize.directive';
import { FieldValidationDirective } from './framework/directives/field-validation.directive';
import { LimitToDirective } from './framework/directives/limit-to.directive';
import { OnlyLetterDirective } from './framework/directives/only-letter.directive';
import { OnlynumberDirective } from './framework/directives/onlynumber.directive';
import { ProgressIndicatorDirective } from './framework/directives/progress-indicator.directive';
import { ReplaceDirective } from './framework/directives/replace.directive';
import { EnableValidationDirective } from './framework/directives/EnableValidation';
import { BoldDirective } from './framework/directives/bold.directive';
import { AppDropdownComponent } from './framework/dropdown/app-dropdown.component';
import { AlertDirective } from './framework/directives/alert.directive';
import { RightAlignDirective } from './framework/directives/right-align.directive';
import { AppFieldComponent } from './framework/field/app-field.component';
import { TableModule } from 'primeng/table';
import { GridPanelModule } from './framework/grid-panel/grid-panel.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        DropdownModule,
        NgLoggerModule.forRoot(Level.LOG)
      ],
    exports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        BrowserAnimationsModule,
        DataTableModule,
        TableModule,
        GridPanelModule,
        DialogModule,
        GrowlModule,
        GridPanelModule,
        AccordionModule,
        CalendarModule,
        InputMaskModule,
        FieldsetModule,
        ConfirmDialogModule,
        HeaderModule,
        EnterKeyModule,
        CustomFormModule,
        FooterModule,
        TooltipModule,
        HttpClientModule,
        DropdownModule,
        ErrorsModule,
        AutoFocusDirective,
        CapitalizeDirective,
        LimitToDirective,
        OnlyLetterDirective,
        OnlynumberDirective,
        ProgressIndicatorDirective,
        ReplaceDirective,
        EnableValidationDirective,
        SignalDirective,
        FunKeyDirective,
        FunctionKeyPanelComponent,
        BaseViewComponent,
        AppFieldComponent,
        BoldDirective,
        AlertDirective,
        RightAlignDirective,
        AppDropdownComponent
    ],
    declarations: [
        AutoFocusDirective,
        CapitalizeDirective,
        FieldValidationDirective,
        LimitToDirective,
        OnlyLetterDirective,
        OnlynumberDirective,
        ProgressIndicatorDirective,
        ReplaceDirective,
        EnableValidationDirective,
        SignalDirective,
        FunKeyDirective,
        FunctionKeyPanelComponent,
        BaseViewComponent,
        AppFieldComponent,
        BoldDirective,
        AlertDirective,
        RightAlignDirective,
        AppDropdownComponent,
    ],
    providers: [
        MessagesService,
        TerminalClientService,
        AuthService,
        ConfirmationService
    ],
    schemas: []
})

export class SharedModule { }
