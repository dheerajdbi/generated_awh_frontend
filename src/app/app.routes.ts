import { MainMenuComponent } from "./framework/main-menu/main-menu.component";
import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./framework/errors/page-not-found/page-not-found.component";
import { NgModule } from "@angular/core";
import { LoginComponent } from "./framework/login/login.component";
import { WaitComponent } from "./framework/wait/wait.component";

export const routeStates: Routes = [
  { path: "", component: LoginComponent },
  { path: "wait", component: WaitComponent },
  { path: "main", component: MainMenuComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routeStates, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutesModule {}
