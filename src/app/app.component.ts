import { OnInit, Component, ViewContainerRef } from "@angular/core";
import {
  ActivatedRoute,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  Router,
  RoutesRecognized,
  RouterEvent,
  NavigationError
} from "@angular/router";
import {
  TerminalClientService,
  TerminalClientState
} from "./framework/services/terminal-client.service";
import { AppService } from "./app.service";
import { MessagesService } from "./framework/services/messages.service";
import { AuthService } from "./framework/services/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";
  public activePanel: any;
  msgs: any;
  waiting: boolean;

  constructor(
    private router: Router,
    private client: TerminalClientService,
    private appService: AppService,
    private msgService: MessagesService,
    private authService: AuthService
  ) {
    this.authService.handleAuthentication();

    this.client.state
      .map<string, boolean>(v => {
        return (
          v === TerminalClientState.SENDING_REPLY ||
          v === TerminalClientState.WAITING_REQUEST ||
          v === TerminalClientState.NOT_CONNECTED ||
          v === TerminalClientState.STALLED
        );
      })
      .distinctUntilChanged()
      .subscribe(v => (this.waiting = v));

    this.msgService.growlMessageObservable.subscribe(messages => {
      this.msgs = messages;
    });

    // router.events.subscribe((event: RouterEvent) => {
    //     this.navigationInterceptor(event);
    // });

    history.pushState(null, null, location.href);
    window.addEventListener("popstate", function(event) {
      history.pushState(null, null, location.href);
    });
  }

  ngOnInit() {
    // scroll to top when there's a router event
    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      if (evt instanceof NavigationStart) {
        this.appService.setLoading(true);
      }
      if (evt instanceof NavigationEnd) {
        this.appService.setLoading(false);
      }
      window.scrollTo(0, 0);
    });
    this.client.state.subscribe(state => {
      if (state !== TerminalClientState.ON_SCREEN) {
        this.appService.setLoading(true);
      } else {
        this.appService.setLoading(false);
      }
    });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.appService.setLoading(true);
    }
    if (event instanceof NavigationEnd) {
      this.appService.setLoading(false);
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.appService.setLoading(false);
    }
    if (event instanceof NavigationError) {
      this.appService.setLoading(false);
    }
  }

  showLoader(): boolean {
    return this.appService.getLoading() && this.authService.isAuthenticated();
  }
}

export class PageGdoObject {
  content: any[] = [];
}
