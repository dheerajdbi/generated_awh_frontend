import { Injectable } from '@angular/core';

@Injectable()
export class AppService {
    loading: boolean;

    constructor() {}

    getLoading() {
        return this.loading;
    }

    setLoading(newValue: boolean) {
        this.loading = newValue;
    }
}
