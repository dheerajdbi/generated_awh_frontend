import { Directive, OnInit } from "@angular/core";
import { Input, HostListener } from "@angular/core";
import {
  TerminalClientService,
  TerminalClientState
} from "../services/terminal-client.service";
import { Logger } from "@nsalaun/ng-logger";

@Directive({
  selector: "[appSignal]"
})
export class SignalDirective implements OnInit {
  @Input("appSignal") appSignal: string;
  @Input("cmdKey") cmdKey: string;
  @Input("field") field: string;
  @Input("isConfirm") isConfirm: boolean;

  constructor(private client: TerminalClientService, private logger: Logger) {}

  ngOnInit() {
    if (this.appSignal) {
      this.logger.debug(`Signal reset: ${this.appSignal}`);
      this.client.setSignal(this.appSignal, false, this.cmdKey);
    }
  }

  @HostListener("click", ["$event.target"])
  onClick(btn) {
    if (this.appSignal) {
      if (this.client.state.getValue() === TerminalClientState.ON_SCREEN) {
        this.logger.debug(`Signal sent: ${this.appSignal}`);
        this.client.setSignal(this.appSignal, true, this.cmdKey, this.field);
        this.client.reply();
      } else {
        this.logger.warn(
          `Terminal is locked. Signal aborted: ${this.appSignal}`
        );
      }
    }
  }
}
