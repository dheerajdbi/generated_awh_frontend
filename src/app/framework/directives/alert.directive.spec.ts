import { TestBed, inject } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { AlertDirective } from './alert.directive';

describe('AlertDirective', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: ElementRef,
                    useValue: {
                        nativeElement: {
                            className: {

                            }
                        }
                    }
                },
            ]
        });
    });

    it('should create an instance', inject([ElementRef], (el) => {
        const directive = new AlertDirective(el);
        expect(directive).toBeDefined();
    }));

    it('should assign alert class name when the input of the directive is assigned', inject([ElementRef], (el) => {
        const directive = new AlertDirective(el);
        directive.appAlert = 'danger';
        spyOn(el.nativeElement, 'className');
        directive.ngOnInit();
        expect(el.nativeElement.className).toContain('alert alert-danger');
    }));

});
