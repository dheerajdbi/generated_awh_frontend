import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appProgressIndicator]'
})
export class ProgressIndicatorDirective {

    constructor(private elRef: ElementRef, private renderer: Renderer2) {
    }

    @HostListener('click') performTask() {
        const li = this.renderer.createElement('div');
        const text = this.renderer.createText(this.elRef.nativeElement.text);
        this.renderer.appendChild(li, text);
        this.renderer.appendChild(this.elRef.nativeElement, li);
    }
}
