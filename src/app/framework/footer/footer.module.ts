import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFooterComponent } from './footer.component';
import { RouterModule, Routes, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ],
    declarations: [
        AppFooterComponent,
    ],
    exports: [
        AppFooterComponent
    ]
})

export class FooterModule { }
