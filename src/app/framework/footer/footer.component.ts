import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

// only requires the version number form package.json
const appVersion = require('../../../../package.json').version;

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css'],
})

export class AppFooterComponent {

    buildNumber = appVersion;
    goToLink = '';
    constructor(private router: Router) {
    }
}
