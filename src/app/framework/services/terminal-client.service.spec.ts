import { TestBed, inject } from '@angular/core/testing';

import { TerminalClientService } from './terminal-client.service';
import { Http, HttpModule } from '@angular/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { MessagesService } from './messages.service';
import { AuthService } from './auth.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule, HttpClient } from '@angular/common/http';

const fakeAuthService = null;
xdescribe('TerminalClientService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            providers: [
                TerminalClientService,
                HttpClient,
                {
                    provide: Router,
                    useClass: class {
                        navigate = jasmine.createSpy('navigate');
                    }
                },
                AuthService,
                MessagesService,
                {
                    provide: AuthService,
                    useClass: class {
                        authenticated = new BehaviorSubject<boolean>(false);
                    }
                },

            ]
        });
    });

    it('should be created', inject([TerminalClientService], (service: TerminalClientService) => {
        expect(service).toBeTruthy();
    }));

    it('should log Client state:NOT_AUTHENTICATED and route to / when not authenticated',
        inject([TerminalClientService, Router, AuthService, MessagesService],
            (service: TerminalClientService, router: Router, authService: AuthService, http: HttpClient, msgService: MessagesService) => {
                spyOn(console, 'log');
                service = new TerminalClientService(http, router, msgService, authService,null);
                expect(console.log).toHaveBeenCalledWith('Client state:NOT_AUTHENTICATED');
                expect(router.navigate).toHaveBeenCalledWith(['/']);
    }));

    it('should request new session and wait when authenticated',
        inject([TerminalClientService, Router, AuthService, HttpClient, MessagesService],
            (service: TerminalClientService, router: Router, authService: AuthService, http: HttpClient, msgService: MessagesService) => {
                spyOn(console, 'log');
                authService.authenticated.next(true);
                spyOn(http, 'post').and.returnValue(Observable.of({ fakeReturn: '' }));
                service = new TerminalClientService(http, router, msgService, authService,null);
                expect(http.post).toHaveBeenCalledWith('http://localhost:8080/clientapi/v1/createSession', {});
                expect(console.log).toHaveBeenCalledWith('Client state:NOT_CONNECTED');
                expect(console.log).toHaveBeenCalledWith('request new session:', '{"fakeReturn":""}');
                expect(router.navigate).toHaveBeenCalledWith(['/wait']);
    }));

    it('should wait for request and log that the status is waiting for request',
        inject([TerminalClientService, Router, AuthService, HttpClient, MessagesService],
            (service: TerminalClientService, router: Router, authService: AuthService, http: HttpClient, msgService: MessagesService) => {
                spyOn(console, 'log');
                authService.authenticated.next(true);
                service = new TerminalClientService(http, router, msgService, authService,null);
                service.session.next({ fakeSession: '' });
                expect(console.log).toHaveBeenCalledWith('Client state:WAITING_REQUEST');
                expect(router.navigate).toHaveBeenCalledWith(['/wait']);
    }));

    describe('#reply', () => {
        it('should check if there is no request and then throw an error message',
            inject([TerminalClientService, Router, AuthService, HttpClient, MessagesService],
                (service: TerminalClientService, router: Router, authService: AuthService, http: HttpClient, msgService: MessagesService) => {
                    spyOn(msgService, 'pushToMessages');
                    expect(function () { service.reply(); }).toThrow(new Error('Cannot reply since there is no request.'));
                    expect(msgService.pushToMessages).toHaveBeenCalledWith('error', 'Error', 'Cannot reply since there is no request.');
        }));
    });

});
