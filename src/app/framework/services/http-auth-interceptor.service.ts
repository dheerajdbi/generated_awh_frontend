import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { AuthService } from './auth.service';
import URI from 'urijs';

@Injectable()
export class HttpAuthInterceptorService implements HttpInterceptor {

    constructor(private authService: AuthService) {
        console.log('Http interceptor created!');
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('Http intercepted!');
        // TODO: Configure me !!
        const finalUrl = 'http://localhost:3000' + req.url;
        if (this.authService.isAuthenticated()) {
            const accessToken = this.authService.authInfo.getValue().accessToken;
            const changedReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${accessToken}`),
                url: finalUrl
            });
            return next.handle(changedReq);
        } else {
            const changedReq = req.clone({
                url: finalUrl
            });
            return next.handle(changedReq);
        }
    }
}
