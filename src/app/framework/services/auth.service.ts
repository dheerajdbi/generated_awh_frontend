import { Injectable } from "@angular/core";
import * as auth0 from "auth0-js";
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AuthService {
  config = require("assets/configuration.json");
  authInfo: BehaviorSubject<any> = new BehaviorSubject<any>(
    this.restoreAuthInfo()
  );
  authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    this.isAuthenticated()
  );
  private authenticatedSub: any;

  auth0 = new auth0.WebAuth({
    clientID: this.config.clientID,
    domain: this.config.domain,
    responseType: this.config.responseType,
    audience: this.config.audience,
    redirectUri: this.config.redirectUri,
    scope: this.config.scope
  });

  constructor(private http: HttpClient) {
    // Every time authinfo change and every seconds, check authentication.
    this.authenticatedSub = Observable.merge(
      this.authInfo,
      Observable.timer(1000)
    )
      .map(() => this.isAuthenticated())
      .subscribe(this.authenticated);
  }

  public login(): void {
    const nonce = this.genNonce();
    localStorage.setItem("auth_nonce", nonce);
    this.auth0.authorize({
      state: "",
      nonce
    });
  }

  public logout(): void {
    this.changeAuthInfo(null);
  }

  public isAuthenticated(): boolean {
    // const authInfo = this.authInfo.getValue();
    // if (authInfo) {
    //     const expiresAt = authInfo.expiresAt;
    //     return new Date().getTime() < expiresAt;
    // } else {
    //     return false;
    // }
    return true;
  }

  public handleAuthentication(): void {
    const nonce = localStorage.getItem("auth_nonce");
    localStorage.removeItem("auth_nonce");
    this.auth0.parseHash(
      {
        state: "",
        nonce
      },
      (err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          window.location.hash = "";
          this.changeAuthInfo(authResult);
        } else if (err) {
          console.log(err);
        }
      }
    );
  }

  private genNonce() {
    const charset =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._~";
    const result = [];
    (<any>window.crypto.getRandomValues(new Uint8Array(32))).forEach(c =>
      result.push(charset[c % charset.length])
    );
    return result.join("");
  }

  private restoreAuthInfo() {
    return JSON.parse(localStorage.getItem("auth_info"));
  }

  private changeAuthInfo(authInfo) {
    if (authInfo) {
      authInfo.expiresAt = JSON.stringify(
        authInfo.expiresIn * 1000 + new Date().getTime()
      );
      localStorage.setItem("auth_info", JSON.stringify(authInfo));
    } else {
      localStorage.removeItem("auth_info");
    }
    this.authInfo.next(authInfo);
  }
}
