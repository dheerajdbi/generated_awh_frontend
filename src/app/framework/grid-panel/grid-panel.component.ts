import { LazyLoadEvent } from "primeng/components/common/lazyloadevent";
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  HostListener
} from "@angular/core";
import { AppDataService } from "../../app.data.service";
import { HttpErrorResponse } from "@angular/common/http/src/response";
import { Table } from "primeng/table";
import { TerminalClientService } from "../services/terminal-client.service";
import { Observable, Subscriber } from "rxjs";

@Component({
  selector: "app-grid-panel",
  templateUrl: "./grid-panel.component.html",
  styleUrls: ["./grid-panel.component.css"]
})
export class GridPanelComponent {
  flatVariable = {};
  totalRecords: any;
  mySelections: any[];
  selectDropDownItem: any[] = [];
  init: boolean = false;
  onSort: boolean = false;
  @ViewChild(Table) turboTable: Table;
  @Input()
  data: {
    header: string;
    onEscapeOption: boolean; // Handle hide function .
    selectionMode?: string;
    data: any;
    totalRecords: number;
    width?: string;
    selectedIndex?: any;
    height?: string;
    rowsDisplayed?: number;
    tablePaginator?: boolean;
    selectionOption?: boolean;
    isDownload?: boolean;
    editedIndexes?: number[];
    dropDownItems?: any[];
    dropdownMatchingItem?: string;
    columns: TableParam[];
    buttons: PopUpButton[];
    flatVariable?: {};
  };

  constructor(private client: TerminalClientService) {}

  @Output() dialogEvent = new EventEmitter();

  onClick($event, signal) {
    this.dialogEvent.emit({ $event, signal });
  }

  hide() {
    if (this.data.onEscapeOption) {
      // this.submit();
    }
  }

  getPage(event: LazyLoadEvent) {
    this.flatVariable = this.data.flatVariable;
    if (this.init) {
      if (this.onSort) {
        this.customSort(event);
        this.turboTable.first =
          this.flatVariable["page"] * this.flatVariable["size"];
        this.onSort = false;
        return;
      }
      this.flatVariable["page"] = event.first / event.rows;
      this.flatVariable["size"] = event.rows;
      this.flatVariable["_SysCmdKey"] = "27";
      this.flatVariable["pageGdo"] = null;
      this.client.reply();
    } else {
      this.init = true;
      this.turboTable.first =
        this.flatVariable["page"] * this.flatVariable["size"];
      this.dropDownFieldFilter();
      this.dialogEvent.emit(this.mySelections);
    }
  }

  dropDownFieldFilter() {
    if (this.data.dropDownItems) {
      this.data.data.forEach((rowData, id) => {
        this.data.dropDownItems.forEach(dropDownItem => {
          if (rowData[this.data.dropdownMatchingItem] === dropDownItem.code) {
            this.selectDropDownItem[id] = dropDownItem;
          }
        });
      }, this);
    }
  }

  customSort(event) {
    this.data.data.sort((t1, t2) => {
      if (event.sortOrder == 1) {
        if (t1[event.sortField] > t2[event.sortField]) {
          this.dropDownFieldFilter();
          return 1;
        }
        if (t1[event.sortField] < t2[event.sortField]) {
          this.dropDownFieldFilter();
          return -1;
        }
      } else {
        if (t1[event.sortField] < t2[event.sortField]) {
          this.dropDownFieldFilter();
          return 1;
        }
        if (t1[event.sortField] > t2[event.sortField]) {
          this.dropDownFieldFilter();
          return -1;
        }
      }
      return 0;
    });
  }

  performFilter(flatVariable) {
    this.data.flatVariable = flatVariable;
    this.getPage(null);
  }

  onRowSelect($event) {
    this.dialogEvent.emit(this.mySelections);
  }

  onRowUnselect($event) {
    this.dialogEvent.emit(this.mySelections);
  }

  setFilterData(filterValue: string, filterColumnName: string) {
    this.flatVariable[filterColumnName] = filterValue;
  }

  changeValue(selected, index, col) {
    this.data.data[index][col] = selected.code;
  }

  openPopModel(index, column) {
    this.dialogEvent.emit({ openPopModel: true, index: index, col: column });
  }
}

interface TableParam {
  field: string;
  header: string;
  editable?: boolean;
  style?: string;
  joinColumn: boolean;
  joinColumnsList?: any;
  hideColumn?: boolean;
  isSearch?: boolean;
  isFilter?: boolean;
  isReadOnly?: boolean;
  isDropDown?: boolean;
  openPopModel?: boolean;
  selection?: any;
  totalRecords?: number;
  paginator?: boolean;
  rows?: string;
  rowPerPageOptions?: string;
  resizableColumns?: boolean;
  loading?: boolean;
}

interface PopUpButton {
  label: string;
  icon: string;
}
