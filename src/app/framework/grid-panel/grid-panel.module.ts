import { FormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes, RouterLink } from "@angular/router";
import {
    DataTableModule,
    DialogModule,
    ButtonModule,
    ConfirmDialogModule
} from "primeng/primeng";
import { TableModule } from "primeng/table";
import { GridPanelComponent } from "./grid-panel.component";

@NgModule({
    imports: [
        CommonModule,
        TableModule,
        DialogModule,
        FormsModule,
        ButtonModule,
        ConfirmDialogModule
    ],
    declarations: [
        GridPanelComponent
    ],
    exports: [
        GridPanelComponent
    ]
})

export class GridPanelModule {}
