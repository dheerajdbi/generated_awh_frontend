import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-awh-field',
  templateUrl: './awh-field.component.html',
  styleUrls: ['./awh-field.component.css']
})
export class RmiFieldComponent implements OnInit {// the size of screen is 80x24
  @Input() fieldParam: FieldParams;
  @Input() formGroup: FormGroup;

  ngOnInit(): void {}
}
export interface FieldParams {
  id: string;
  x?: string; y?: string; width?: string;
  divclass?: string;
  class?: string;
  formCtrl?: string;
  type?: string;
  name?: string;
  for?: string;
  label?: string;
  validationFn?: ValidatorFn;
  value?: string;
  readonly?: boolean;
}
