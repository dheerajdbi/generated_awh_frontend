import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { element } from 'protractor';
import { FieldParams } from './awh-field/awh-field.component';

@Component({
    selector: 'app-custom-form',
    templateUrl: './custom-form.component.html',
    styleUrls: ['./custom-form.component.css'],
})

export class CustomFormComponent implements OnInit {
    @Input() formContents: FieldParams[];
    @Output() formEvent = new EventEmitter();
    myform: FormGroup;
    formContentsSorted: any[][];


    get value() { return this.myform.value; }


    constructor(private fb: FormBuilder) {
    }

    onFormSubmit() { this.formEvent.emit({ 'value': this.myform.value, 'valid': this.myform.valid }); }

    ngOnInit() {

        this.myform = new FormGroup({});
        this.formContentsSorted = this.x_to_col(this.rmi_sort(this.formContents));
        this.formContents.forEach(formContent => {
            if (formContent.type === 'input') {
                this.myform.addControl(formContent.formCtrl, this.fb.control(formContent.value, formContent.validationFn));
            }
        });
    }

  rmi_sort(arr: any[]): any {
      const arr_1 = arr.sort((a, b) => Number(a.y) - Number(b.y));
      const arr_2 = [];
      for (let i = 0; i < arr_1[arr_1.length - 1].y; i++) {
          arr_2[i] = arr_1.filter((f) => f.y === (i + 1).toString());
          arr_2[i] = arr_2[i].sort((a, b) => Number(a.x) - Number(b.x));
      }
      return arr_2;
  }

  x_to_col(arr: any[][]): any {
      arr.forEach((el, index) => {
          el.forEach((e, i) => {
              if (e.width === undefined) {
                  e.width = '1';
              }
              e.divclass = 'col-md-' + e.width + ' col-md-push-' + e.x;
          });
      });
      return arr;
  }

}
