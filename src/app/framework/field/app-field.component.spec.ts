import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { style } from '@angular/animations';
import { By } from '@angular/platform-browser';
import { AppFieldComponent } from './app-field.component';

describe('AppFieldComponent', () => {
    let component: AppFieldComponent = new AppFieldComponent;
    let fixture: ComponentFixture<AppFieldComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, FormsModule],
            declarations: [AppFieldComponent]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppFieldComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should disable input when disabled is true', () => {
        component.disabled = true;
        fixture.detectChanges();
        const input = fixture.debugElement.query(By.css('input'));
        expect(input.attributes['ng-reflect-is-disabled']).toBeTruthy();
    });

    it('should change field color when color attribute is assigned', () => {
        component.color = 'green';
        fixture.detectChanges();
        const input = fixture.debugElement.query(By.css('input'));
        expect(input.nativeElement.style.color).toEqual('green');
    });

    describe('uint', () => {
        it('should be invalid on 123a', () => {
            component.type = 'uint';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '123a';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });
        });

        it('should be valid on 123', () => {
            component.type = 'uint';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '123';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-valid');
            });
        });

    });

    describe('snum', () => {
        it('should be invalid on 1.', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1.';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });
        });

        it('should be invalid on 1,', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1,';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });
        });

        it('should be valid on 1,000.02', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1,000.02';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-valid');
            });

        });

        it('should not end with a + sign', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1,000.02+';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });

        });

        it('should not end with a - sign', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1,000.02-';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });

        });

        it('should be valid if it starts with a + sign', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '+1,000.02';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-valid');
            });

        });

        it('should be valid if it starts with a - sign', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '-1,000.02';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-valid');
            });

        });

        it('should be valid if it starts with a .', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '.02';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-valid');
            });

        });

        it('should be invalid if there are commas after the decimal point', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '.02,00';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });

        });

        it('should be valid only if there are 3 digits after a comma', () => {
            component.type = 'snum';
            component.ngOnChanges();
            fixture.whenStable().then(() => {
                const input = fixture.debugElement.query(By.css('input'));
                const el = input.nativeElement;
                el.value = '1,00';
                el.dispatchEvent(new Event('input'));
                fixture.detectChanges();
                expect(el.className).toContain('ng-invalid');
            });

        });

    });

    describe('hide leading zeroes', () => {
        it('should hide leading zeroes', () => {
            component.value = '0.01';
            component.hideLeadingZeroes = true;
            component.ngOnChanges();
            expect(component.value).toEqual('.01');
        });
        it('should hide leading zeroes even if sign is present', () => {
            component.value = '-0.01';
            component.hideLeadingZeroes = true;
            component.ngOnChanges();
            expect(component.value).toEqual('-.01');
        });
    });

    describe('hide sign', () => {
        it('should hide positive sign', () => {
            component.value = '+0.01';
            component.hideSign = true;
            component.ngOnChanges();
            expect(component.value).toEqual('0.01');
        });
        it('should hide negative sign', () => {
            component.value = '-0.01';
            component.hideSign = true;
            component.ngOnChanges();
            expect(component.value).toEqual('0.01');
        });
    });

    describe('hide zeroes in decimal', () => {
        it('should hide decimal if it is equal zero', () => {
            component.value = '-1.000';
            component.hideDecimalIfZero = true;
            component.ngOnChanges();
            expect(component.value).toEqual('-1');
        });
        it('should not hide decimal if it is not equal zero', () => {
            component.value = '-1.003';
            component.hideDecimalIfZero = true;
            component.ngOnChanges();
            expect(component.value).toEqual('-1.003');
        });
    });

});
