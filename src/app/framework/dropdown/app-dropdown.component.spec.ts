import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppDropdownComponent } from './app-dropdown.component';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventEmitter } from '@angular/core';

describe('AppDropdownComponent', () => {
    let component: AppDropdownComponent;
    let fixture: ComponentFixture<AppDropdownComponent>;
    const doptions = [
        { label: 'Audi', value: 'Audi' },
        { label: 'BMW', value: 'BMW' },
        { label: 'Mercedes', value: 'Mercedes' }
    ];
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, CommonModule, DropdownModule, FormsModule, BrowserAnimationsModule],
            declarations: [AppDropdownComponent]
        })
          .compileComponents().then(() => {
              fixture = TestBed.createComponent(AppDropdownComponent);
              component = fixture.componentInstance;
          });
    }));

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should have select', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: false,
            editableFlag: false,
            width: '10em'
        };
        fixture.detectChanges();
        const select = fixture.debugElement.query(By.css('select'));
        expect(select).toBeTruthy();
    });

    it('should have options', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: false,
            editableFlag: false,
            width: '10em'
        };
        fixture.detectChanges();
        const options = fixture.debugElement.queryAll(By.css('option'));
        const els = [];

        for (let i = 0; i < options.length; i++) {
            els.push({ 'label': options[i].nativeElement.innerText, 'value': options[i].nativeElement.value });
        }
        expect(els).toEqual(doptions);
    });

    it('should be valid if width is changed', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: false,
            editableFlag: false,
            width: '20em'
        };
        fixture.detectChanges();
        const dropdown = fixture.debugElement.query(By.css('.ui-dropdown'));
        const width = dropdown.nativeElement;
        expect(width.style.width).toBe('20em');
    });

    it('should be valid if editFlag is true', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: false,
            editableFlag: true,
            width: '10em'
        };
        fixture.detectChanges();
        const input = fixture.debugElement.query(By.css('.ui-inputtext'));
        const readonly = input.nativeElement;
        expect(readonly).toBeTruthy();
    });

    it('should be valid if disableFlag is true', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: true,
            editableFlag: false,
            width: '10em'
        };
        fixture.detectChanges();
        const input = fixture.debugElement.query(By.css('input, .ng-tns-c3-0'));
        const disable = input.nativeElement.disabled;
        expect(disable).toBeTruthy();
    });

    it('should emit changeEvent', () => {
        component.dropdownParams = {
            doptions: doptions,
            disableFlag: false,
            editableFlag: false,
            width: '10em'
        };
        fixture.detectChanges();
        const fakeEventEmitter = new EventEmitter();
        component.changeEvent = fakeEventEmitter;
        spyOn(component.changeEvent, 'emit');
        component.change();
        expect(component.changeEvent.emit).toHaveBeenCalled();
    });

});
