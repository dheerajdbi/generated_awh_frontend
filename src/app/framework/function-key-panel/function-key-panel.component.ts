import {
    Component,
    Input,
    Output,
    EventEmitter,
    HostListener
} from "@angular/core";

@Component({
    selector: "app-function-key-panel",
    templateUrl: "./function-key-panel.component.html",
    styleUrls: ["./function-key-panel.component.css"]
})

export class FunctionKeyPanelComponent {
    @Output() FunctionKey = new EventEmitter();
    @Input() keys: FunctionKey[];
    @Input() field: string;

    constructor() {}
}

export interface FunctionKey {
    id: string;
    btnStyle?: string;
    btnTitle: string;
    signal: string;
    display: boolean;
    cmdKey?:string;
    isConfirm?:false;
}
