import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalVariable } from './../services/global-variables';

@Injectable()
export class StateManager {

    constructor(private globalVariable: GlobalVariable, private router: Router) {
    }

    saveState(step, pgmState) {
        if (step == null) {
            return;
        }

        this.globalVariable.$previousMenu = this.router.url.substr(1);
        this.globalVariable.$activeMenu = step;
        this.globalVariable.$flatVariable = null;
        localStorage.setItem('PGMSTATE', JSON.stringify({
            step: step,
            globalVariable: Object.assign({}, this.globalVariable)
        }));

        if (pgmState) {
            this.globalVariable.$flatVariable = pgmState;
        }
    }

    getState(step) {
        const pgmState = JSON.parse(localStorage.getItem('PGMSTATE'));

        if (pgmState) {
            this.globalVariable.$previousMenu = pgmState.globalVariable.previousMenu;
            this.globalVariable.$activeMenu = pgmState.globalVariable.activeMenu;
            this.globalVariable.$companyNumber = pgmState.globalVariable.companyNumber;
            this.globalVariable.$districtNumber = pgmState.globalVariable.districtNumber;
            this.globalVariable.$authenticated = pgmState.globalVariable.authenticated;
        }
        this.globalVariable.$locale = localStorage.getItem('locale');
    }
}
