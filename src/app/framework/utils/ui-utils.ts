import { Injectable } from '@angular/core';

@Injectable()
export class UiUtils {

    display = false;
    width = 300;

    openModal() {
        this.display = true;
    }

    closeActiveModal() {
        this.display = false;
    }

    isModal(nextStep) {
        const isModal = nextStep && nextStep.stepAttributes['modalScreen'] === 'true';
    }

    replaceEndString(strSource: string, strToReplace: string, strToReplaceWith: string) {
        // If the string source is null then simply return the strSource
        const len = strSource.length;

        if (len === 0) {
            return strSource;
        }

        // If the string to replace is null then simply return the strSource
        if (strToReplace == null || strToReplace === '') {
            return strSource;
        }

        // If the string to replace with is null then simply return the
        // strSource Or if strToReplaceWith is same as strToReplace then
        // simply return the strSource
        if (strToReplaceWith == null || strToReplace === strToReplaceWith) {
            return strSource;
        }

        if (!strSource.endsWith(strToReplace)) {
            return strSource;
        }

        return strSource.substring(0, len - strToReplace.length) + strToReplaceWith;
    }
}
