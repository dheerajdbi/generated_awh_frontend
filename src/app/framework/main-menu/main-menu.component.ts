import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TerminalClientService } from '../services/terminal-client.service';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  main={};
  constructor(private client: TerminalClientService, private msgService: MessagesService) { }

  ngOnInit() {
   this.main = this.client.getModel();
  }
  

  gotoEquipment(val){
    this.main["option"] = val;
    this.client.reply();
  }

}
