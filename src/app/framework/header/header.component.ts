import { Component, Input, Output } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class AppHeaderComponent {
  @Input() signoutDisable: boolean;
  @Input() frenchDisable: boolean;
  @Input() publicBulletinDisable: boolean;
  @Input() previous: string;
  @Input() previousLocation: boolean;
  @Input() previousDisplay = true;
  @Input() endJob: string;
  @Input() endJobHide = true;
  @Input() menuName: string;
  @Input() programName: string;
  @Input() conditionName: string;
  @Input() isProgram: boolean;
  public today: number = Date.now();

  constructor(private location: Location, private authService: AuthService) {
  }
  goPrevious() {
    this.location.back();
  }
  signout() {
    if (!this.signoutDisable) {
      this.authService.logout();
    }
  }
}
