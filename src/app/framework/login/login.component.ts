import { Component, OnInit, HostListener } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import "rxjs/Rx";
import { Message } from "primeng/primeng";
import { InputTextModule } from "primeng/primeng";
import { AuthService } from "../services/auth.service";
import { Logger } from "@nsalaun/ng-logger";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"]
})

export class LoginComponent implements OnInit {
    private _title = "AWH - ALL";
    private _name = "";

    model: any = {};
    returnUrl: String;
    errorMsg: Message[] = [];

    constructor(private router: Router, private authService: AuthService) {}

    ngOnInit() {}

    login() {
        this.authService.login();
    }
}
