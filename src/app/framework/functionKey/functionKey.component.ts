import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-functionkey',
    templateUrl: './functionKey.component.html',
})

export class FunctionKeyComponent {
    @Input() B1 = false;
    @Input() B2 = false;
    @Input() B3 = false;
    @Input() B4 = false;
    @Input() B5 = false;
    @Input() B6 = false;
    @Input() B7 = false;
    @Input() B8 = false;
    @Input() B9 = false;
    @Input() B10 = false;
    @Input() B11 = false;
    @Input() B12 = false;
    @Input() B13 = false;
    @Input() B14 = false;
    @Input() B15 = false;
    @Input() B16 = false;
    @Input() B17 = false;
    @Input() B18 = false;
    @Input() B19 = false;
    @Input() B20 = false;
    @Input() B21 = false;
    @Input() B22 = false;
    @Input() B23 = false;
    @Input() B24 = false;

    // if it goes to another page
    @Input() B1link: string;
    @Input() B2link: string;
    @Input() B3link: string;
    @Input() B4link: string;
    @Input() B5link: string;
    @Input() B6link: string;
    @Input() B7link: string;
    @Input() B8link: string;
    @Input() B9link: string;
    @Input() B10link: string;
    @Input() B11link: string;
    @Input() B12link: string;
    @Input() B13link: string;
    @Input() B14link: string;
    @Input() B15link: string;
    @Input() B16link: string;
    @Input() B17link: string;
    @Input() B18link: string;
    @Input() B19link: string;
    @Input() B20link: string;
    @Input() B21link: string;
    @Input() B22link: string;
    @Input() B23link: string;
    @Input() B24link: string;

    // if it goes to a method
    @Output() B1function = new EventEmitter();
    @Output() B2function = new EventEmitter();
    @Output() B3function = new EventEmitter();
    @Output() B4function = new EventEmitter();
    @Output() B5function = new EventEmitter();
    @Output() B6function = new EventEmitter();
    @Output() B7function = new EventEmitter();
    @Output() B8function = new EventEmitter();
    @Output() B9function = new EventEmitter();
    @Output() B10function = new EventEmitter();
    @Output() B11function = new EventEmitter();
    @Output() B12function = new EventEmitter();
    @Output() B13function = new EventEmitter();
    @Output() B14function = new EventEmitter();
    @Output() B15function = new EventEmitter();
    @Output() B16function = new EventEmitter();
    @Output() B17function = new EventEmitter();
    @Output() B18function = new EventEmitter();
    @Output() B19function = new EventEmitter();
    @Output() B20function = new EventEmitter();
    @Output() B21function = new EventEmitter();
    @Output() B22function = new EventEmitter();
    @Output() B23function = new EventEmitter();
    @Output() B24function = new EventEmitter();

    @Input() B1title: string;
    @Input() B2title: string;
    @Input() B3title: string;
    @Input() B4title: string;
    @Input() B5title: string;
    @Input() B6title: string;
    @Input() B7title: string;
    @Input() B8title: string;
    @Input() B9title: string;
    @Input() B10title: string;
    @Input() B11title: string;
    @Input() B12title: string;
    @Input() B13title: string;
    @Input() B14title: string;
    @Input() B15title: string;
    @Input() B16title: string;
    @Input() B17title: string;
    @Input() B18title: string;
    @Input() B19title: string;
    @Input() B20title: string;
    @Input() B21title: string;
    @Input() B22title: string;
    @Input() B23title: string;
    @Input() B24title: string;

    @Input() B1class: string;
    @Input() B2class: string;
    @Input() B3class: string;
    @Input() B4class: string;
    @Input() B5class: string;
    @Input() B6class: string;
    @Input() B7class: string;
    @Input() B8class: string;
    @Input() B9class: string;
    @Input() B10class: string;
    @Input() B11class: string;
    @Input() B12class: string;
    @Input() B13class: string;
    @Input() B14class: string;
    @Input() B15class: string;
    @Input() B16class: string;
    @Input() B17class: string;
    @Input() B18class: string;
    @Input() B19class: string;
    @Input() B20class: string;
    @Input() B21class: string;
    @Input() B22class: string;
    @Input() B23class: string;
    @Input() B24class: string;

    onClick($event, button: string) {
        switch (button) {
            case 'B1':
                this.B1function.emit($event);
                break;
            case 'B2':
                this.B2function.emit($event);
                break;
            case 'B3':
                this.B3function.emit($event);
                break;
            case 'B4':
                this.B4function.emit($event);
                break;
            case 'B5':
                this.B5function.emit($event);
                break;
            case 'B6':
                this.B6function.emit($event);
                break;
            case 'B7':
                this.B7function.emit($event);
                break;
            case 'B8':
                this.B8function.emit($event);
                break;
            case 'B9':
                this.B9function.emit($event);
                break;
            case 'B10':
                this.B10function.emit($event);
                break;
            case 'B11':
                this.B11function.emit($event);
                break;
            case 'B12':
                this.B12function.emit($event);
                break;
            case 'B13':
                this.B13function.emit($event);
                break;
            case 'B14':
                this.B14function.emit($event);
                break;
            case 'B15':
                this.B15function.emit($event);
                break;
            case 'B16':
                this.B16function.emit($event);
                break;
            case 'B17':
                this.B17function.emit($event);
                break;
            case 'B18':
                this.B18function.emit($event);
                break;
            case 'B19':
                this.B19function.emit($event);
                break;
            case 'B20':
                this.B20function.emit($event);
                break;
            case 'B21':
                this.B21function.emit($event);
                break;
            case 'B22':
                this.B22function.emit($event);
                break;
            case 'B23':
                this.B23function.emit($event);
                break;
            case 'B24':
                this.B24function.emit($event);
                break;
        }
    }

    constructor() {
    }
}
