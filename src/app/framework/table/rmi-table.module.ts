import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { SharedModule } from '../../shared.module';
import {
    InputTextModule,
    ButtonModule,
    DialogModule,
    GrowlModule,
    AccordionModule,
    CalendarModule,
    InputMaskModule,
    FieldsetModule,
    DataListModule
} from 'primeng/primeng';
import {
    TableModule
} from 'primeng/table';
import { RMITableComponent } from './rmi-table.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        TableModule,
        FormsModule,
    ],
    declarations: [
        RMITableComponent,
    ],
    exports: [
        RMITableComponent
    ]
})

export class RMITableModule { }
