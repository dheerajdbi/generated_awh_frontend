import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
    selector: 'app-enter-key',
    templateUrl: './enter-key.component.html',
    styleUrls: ['./enter-key.component.css']
})

export class EnterKeyComponent implements OnInit {

    @Input() divClass: string;
    @Input() enterButton: boolean;
    @Input() enterButtonClass: string;
    @Input() enterButtonID: string;
    @Output() enterButtonFunction = new EventEmitter();
    @Input() enterButtonTitle: string;

    constructor() {

    }

    enterClicked($event) {
        this.enterButtonFunction.emit($event);
    }

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvents(event: KeyboardEvent) {
        const keyCode = event.keyCode || event.which;
        switch (keyCode) {
            case 13:
                event.preventDefault();
                event.stopPropagation();
                this.enterButtonFunction.emit();
                break;
        }
    }

    ngOnInit() {
    }

}
