
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { Routes, RouterModule } from '@angular/router';

import { EdtEquipmentTypesComponent} from './edtequipmenttypes/edtEquipmentTypes.component';
import { SelEquipmentTypeComponent} from './selequipmenttype/selEquipmentType.component';
import { PmtForEquipmentBkdwnComponent} from './pmtforequipmentbkdwn/pmtForEquipmentBkdwn.component'; 
 
export const ROUTES: Routes = [
    {
        path:'edtEquipmentTypes',
        component: EdtEquipmentTypesComponent
    },
    {
        path:'selEquipmentType',
        component: SelEquipmentTypeComponent
    },
    {
        path:'pmtForEquipmentBkdwn',
        component: PmtForEquipmentBkdwnComponent
    },
];

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(ROUTES)
	],

	declarations: [
		EdtEquipmentTypesComponent,
		SelEquipmentTypeComponent,
		PmtForEquipmentBkdwnComponent,
	],

	exports: [
		EdtEquipmentTypesComponent,
		SelEquipmentTypeComponent,
		PmtForEquipmentBkdwnComponent,
	]
})

export class EquipmentTypeModule {}