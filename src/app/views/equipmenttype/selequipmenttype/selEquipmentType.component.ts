
import { Component, OnInit, ViewChild  } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
import { PageGdoObject } from '../../../app.component';
import { Table } from 'primeng/table';


@Component({
    selector: 'app-selEquipmentType',
    templateUrl: './selEquipmentType.component.html'
})

export class SelEquipmentTypeComponent implements OnInit {
    public equipmentType: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;
    onSort: boolean = false;
    @ViewChild(Table) turboTable: Table;
 
    constructor(private client: TerminalClientService) {
        this.funcParams = [
            { id: 'fKey-01', btnTitle: 'Ok', signal: 'funKey01', display: true },
            { id: 'fKey-03', btnTitle: 'Cancel', signal: 'funKey03', display: true }
        ];

        this.cols = [
			{ field: 'equipmentTypeCode', header: 'Equipment Type Code', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'equipmentTypeDesc', header: 'Equipment Type Desc', editable: true, isReadOnly: true },
        ];

        this.equipmentType = client.getModel();
    }

    ngOnInit() {
    	if (this.equipmentType.pageGdo) {
    		this.gridVariable = this.equipmentType.pageGdo.content;
    		this.totalRecords = this.equipmentType.pageGdo.totalElements;
    	}
    }

    processGrid(event) {
        if (this.init) {
            if (this.onSort) {
                this.customSort(event);
                this.turboTable.first = this.equipmentType['page'] * this.equipmentType['size'];
                this.onSort = false;
                return;
            }
            this.equipmentType['page'] = event.first / event.rows;
            this.equipmentType['size'] = event.rows;
            this.equipmentType['gdo'] = null;
            this.equipmentType['_SysCmdKey'] = '08';
            this.equipmentType.pageGdo = new PageGdoObject();
            this.onSubmit(3);
        } else {
            this.init = true;
            this.turboTable.first = this.equipmentType['page'] * this.equipmentType['size'];
        }
    }
 
    customSort(event) {
        this.equipmentType.pageGdo.content.sort((t1, t2) => {
            if (event.sortOrder === 1) {
                if (t1[event.sortField] > t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] < t2[event.sortField]) {
                    return -1;
                }
            } else {
                if (t1[event.sortField] < t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] > t2[event.sortField]) {
                    return -1;
                }
            }
            return 0;
        });
    }

    onSubmit(val) {
        if (val === 1) {
            if (this.mySelections !== undefined && this.mySelections !== null && this.mySelections) {
//                this.equipmentType['gdo'] = this.mySelections;
				this.equipmentType['equipmentTypeCode'] = this.mySelections['equipmentTypeCode'];
            }
            else {
                this.displayNoSelection = true;
            }
        }
        else if (val === 2) {
            this.equipmentType['gdo'] = null;
            this.equipmentType['_SysCmdKey'] = '03';
    	}
        this.equipmentType.pageGdo = new PageGdoObject();
        this.client.reply();
    }
}
