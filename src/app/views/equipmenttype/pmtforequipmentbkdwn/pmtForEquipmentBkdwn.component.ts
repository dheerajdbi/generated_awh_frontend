import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-pmtForEquipmentBkdwn',
    templateUrl: './pmtForEquipmentBkdwn.component.html'
})
    
export class PmtForEquipmentBkdwnComponent implements OnInit {
    equipmentType: any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    printEmailOrDisplay = [
		{ value: '_P', code: 'P', description: 'P - Print' },
		{ value: '_E', code: 'E', description: 'E - Email' },
		{ value: '_D', code: 'D', description: 'D - Display' },
	]

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
        this.dialogData = {
            header: 'Pmt for Equipment Bkdwn',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
				{ field: 'equipmentTypeDesc', header: 'Equipment Type Desc', isReadOnly: true },
				{ field: 'selected', header: 'Selected', isReadOnly: true },
            ]
        };
    }

    ngOnInit() {
        this.equipmentType = this.client.getModel();
        this.dialogData.data = this.equipmentType.pageGdo.content;
    }

    dialogEvent($event) {
        this.mySelections = $event;
    }
    
    onSubmit() {
        this.equipmentType['_SysCmdKey'] = '00';
        this.equipmentType.pageGdo = new PageGdoObject();
        this.equipmentType.pageGdo.content = this.mySelections;
        this.client.reply();
    }
}
