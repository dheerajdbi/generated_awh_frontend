import { GridPanelComponent } from "../../../framework/grid-panel/grid-panel.component";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
import { ActivatedRoute } from "@angular/router";
import { deepEqual } from "assert";
import { PageGdoObject } from "../../../app.component";

@Component({
  selector: "app-edtEquipmentTypes",
  templateUrl: "./edtEquipmentTypes.component.html"
})
export class EdtEquipmentTypesComponent implements OnInit {
  equipmentType: any;
  public mySelections: any[] = [];
  public loader = false;
  public mode: string;
  consts: {};
  gdo: any;
  public flatVariable: any;
  public dialogData: any;
  public searchParams: FunctionKey[];
  public funcParams: FunctionKey[];

  constructor(
    private client: TerminalClientService,
    private msgService: MessagesService
  ) {
    this.funcParams = [
      {
        id: "fKey-03",
        btnTitle: "Exit",
        signal: "funKey03",
        display: true,
        cmdKey: "03"
      },
      {
        id: "fKey-06",
        btnTitle: "Go",
        signal: "funKey06",
        display: true,
        cmdKey: "06"
      },
      {
        id: "fKey-12",
        btnTitle: "Cancel",
        signal: "funKey12",
        display: true,
        cmdKey: "12"
      }
    ];
    this.searchParams = [
      {
        id: "fKey-04",
        btnTitle: "Prompt",
        signal: "funKey04",
        display: true,
        cmdKey: "04"
      }
    ];
  }

  dialogEvent($event) {
    this.mySelections = $event;
    this.mySelections.forEach((routeParams, idx) => {
      routeParams["_SysSelected"] =
        this.equipmentType._SysProgramMode === "CHG" ? "CHANGE" : "ADD";
      routeParams["_SysRecordDataChanged"] = "Y";
      this.mySelections[idx] = routeParams;
    });
  }

  ngOnInit() {
    this.equipmentType = this.client.getModel();
    this.dialogData = {
      header: "Edt Equipment Types",
      onEscapeOption: true,
      selectionMode: "multiple",
      isDownload: false,
      selectionOption: false,
      rowsDisplayed: this.equipmentType.size,
      dropdownMatchingItem: "equipReadingUnit",
      tablePaginator: true,
      flatVariable: this.equipmentType,
      totalRecords: this.equipmentType.pageGdo.totalElements,
      data: this.equipmentType.pageGdo.content,
      dropDownItems: [
        { value: "_hrs", code: "hrs", description: "hrs - Hours" },
        { value: "_km", code: "km", description: "km - Kilometres" },
        { value: "_tn", code: "tn", description: "tn - Tonnes" },
        { value: "_kg", code: "kg", description: "kg - Kilograms" },
        { value: "_kWh", code: "kWh", description: "kWh - Kilowatt-hour" },
        { value: "_MJ", code: "MJ", description: "MJ - Megajoules" }
      ],
      columns: [
        {
          field: "equipmentTypeCode",
          header: "Equipment Type Code",
          isFilter: true,
          editable: true
        },
        {
          field: "equipmentTypeDesc",
          header: "Equipment Type Desc",
          isFilter: true,
          editable: true
        },
        {
          field: "equipReadingUnit",
          header: "Equip Reading Unit",
          isFilter: true,
          editable: true,
          isDropDown: true
        }
      ]
    };
    if (this.equipmentType["_SysProgramMode"] == "ADD") {
      this.funcParams[1].btnTitle = "Change";
      this.dialogData.columns[0].editable = true;
    } else if (this.equipmentType["_SysProgramMode"] == "CHG") {
      this.funcParams[1].btnTitle = "Add";
      this.dialogData.columns[0].editable = false;
    }
  }

  process(selected) {
    if (!this.mySelections || this.mySelections.length === 0) {
      this.msgService.pushToMessages(
        "info",
        "Please select a record ",
        "Please select a record "
      );
      return;
    }
    this.mySelections.forEach((routeParams, idx) => {
      routeParams["_SysSelected"] = selected;
      this.mySelections[idx] = routeParams;
    });
    this.equipmentType._SysProgramMode = "DEL";
    this.onSubmit();
  }

  onSubmit() {
    this.equipmentType["_SysCmdKey"] = "00";
    this.equipmentType.pageGdo["content"] = this.mySelections;
    this.client.reply();
  }

  ngDoCheck() {
    if (this.client.$sysConfirmPrompt) {
      this.client.confirm("CNF");
    }
  }
}
