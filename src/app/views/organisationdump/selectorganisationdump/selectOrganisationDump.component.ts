import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import {
  TerminalClientService,
  TerminalClientState
} from "../../../framework/services/terminal-client.service";
import { PageGdoObject } from "../../../app.component";
import { Table } from "primeng/table";

@Component({
  selector: "app-selectOrganisationDump",
  templateUrl: "./selectOrganisationDump.component.html"
})
export class SelectOrganisationDumpComponent implements OnInit {
  @Input() display = false;
  @ViewChild(Table) turboTable: Table;
  public organisationDump: any;
  public funcParams: FunctionKey[];
  gridVariable: any;
  totalRecords;
  mySelections;
  visible = true;
  cols = [];
  init: boolean = false;
  onSort: boolean = false;
  client: TerminalClientService;

  constructor(client: TerminalClientService) {
    this.client = client;
    this.funcParams = [
      {
        id: "fKey-03",
        btnTitle: "Cancel",
        signal: "funKey03",
        display: true,
        cmdKey: "03"
      }
    ];

    this.cols = [
      {
        field: "organisation",
        header: "Organisation",
        editable: true,
        isReadOnly: true,
        isSearch: true
      },
      {
        field: "organisationName50a",
        header: "Organisation Name 50A",
        editable: true,
        isReadOnly: true,
        isSearch: true
      }
    ];

    this.organisationDump = client.getModel();
  }

  ngOnInit() {
    if (this.organisationDump.pageGdo) {
      this.gridVariable = this.organisationDump.pageGdo.content;
      this.totalRecords = this.organisationDump.pageGdo.totalElements;
    }
  }

  processGrid(event) {
    if (this.init) {
      if (this.onSort) {
        this.customSort(event);
        this.turboTable.first =
          this.organisationDump["page"] * this.organisationDump["size"];
        this.onSort = false;
        return;
      }
      this.organisationDump["page"] = event.first / event.rows;
      this.organisationDump["size"] = event.rows;
      this.organisationDump["_SysCmdKey"] = "27";
      this.onSubmit();
    } else {
      this.init = true;
      this.turboTable.first =
        this.organisationDump["page"] * this.organisationDump["size"];
    }
  }

  customSort(event) {
    this.organisationDump.pageGdo.content.sort((t1, t2) => {
      if (event.sortOrder === 1) {
        if (t1[event.sortField] > t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] < t2[event.sortField]) {
          return -1;
        }
      } else {
        if (t1[event.sortField] < t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] > t2[event.sortField]) {
          return -1;
        }
      }
      return 0;
    });
  }

  onSubmit() {
    this.organisationDump["_SysCmdKey"] = "00";
    if (this.mySelections) {
      this.organisationDump["_SysEntrySelected"] = "Y";
      this.organisationDump.organisation = this.mySelections.organisation;
    } else {
      this.organisationDump["_SysEntrySelected"] = "";
    }
    this.client.reply();
  }
  ngDoCheck() {
    if (this.client.$display) {
      this.organisationDump = this.client.getModel();
      this.gridVariable = this.organisationDump.pageGdo.content;
      this.totalRecords = this.organisationDump.pageGdo.totalElements;
    }
  }
  ngOnChange() {
    this.gridVariable = this.organisationDump.pageGdo.content;
    this.totalRecords = this.organisationDump.pageGdo.totalElements;
  }
}
