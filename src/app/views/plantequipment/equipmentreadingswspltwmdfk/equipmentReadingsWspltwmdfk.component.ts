import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-equipmentReadingsWspltwmdfk',
    templateUrl: './equipmentReadingsWspltwmdfk.component.html'
})
    
export class EquipmentReadingsWspltwmdfkComponent implements OnInit {
    plantEquipment: any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	]

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-10', btnTitle: 'Full', signal: 'funKey10', display: true, cmdKey: '10' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
        this.dialogData = {
            header: 'Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
				{ field: 'awhBuisnessSegment', header: 'AWH Buisness Segment', isReadOnly: true, isSearch: true },
				{ field: 'equipmentTypeCode', header: 'Equipment Type Code', isReadOnly: true, isSearch: true },
				{ field: 'displayPlantEquipCode', header: 'Display Plant equip Code', isReadOnly: true },
				{ field: 'equipmentDescriptionDrv', header: 'Equipment Description Drv', isReadOnly: true },
				{ field: 'readingValue', header: 'Reading Value', isFilter: true },
				{ field: 'equipReadingUnit', header: 'Equip Reading Unit', isReadOnly: true },
				{ field: 'previousReadingValue', header: 'Previous Reading Value', isReadOnly: true },
            ]
        };
    }

    ngOnInit() {
        this.plantEquipment = this.client.getModel();
        this.dialogData.data = this.plantEquipment.pageGdo.content;
    }

    dialogEvent($event) {
        this.mySelections = $event;
    }
    
    onSubmit() {
        this.plantEquipment['_SysCmdKey'] = '00';
        this.plantEquipment.pageGdo = new PageGdoObject();
        this.plantEquipment.pageGdo.content = this.mySelections;
        this.client.reply();
    }
}
