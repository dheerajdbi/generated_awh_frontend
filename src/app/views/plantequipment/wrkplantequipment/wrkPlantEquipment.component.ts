import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-wrkPlantEquipment',
    templateUrl: './wrkPlantEquipment.component.html'
})
    
export class WrkPlantEquipmentComponent implements OnInit {
    plantEquipment: any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	]
readingType = [
		{ value: '_ST', code: 'ST', description: 'ST - Standard Reading' },
		{ value: '_SV', code: 'SV', description: 'SV - Service Reading' },
		{ value: '_TO', code: 'TO', description: 'TO - Transfer Out' },
		{ value: '_TI', code: 'TI', description: 'TI - Transfer In Reading' },
		{ value: '_IL', code: 'IL', description: 'IL - Initial reading' },
		{ value: '_RT', code: 'RT', description: 'RT - Retirement' },
		{ value: '_NE', code: 'NE', description: 'NE - Any' },
		{ value: '_MR', code: 'MR', description: 'MR - Meter Reset' },
		{ value: '_UA', code: 'UA', description: 'UA - Make UnAvailable' },
		{ value: '_AV', code: 'AV', description: 'AV - Make Available' },
	]

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'Add', signal: 'funKey06', display: true, cmdKey: '06' },
			{ id: 'fKey-10', btnTitle: 'Visual/Leaser', signal: 'funKey10', display: true, cmdKey: '10' },
			{ id: 'fKey-11', btnTitle: 'Toggle', signal: 'funKey11', display: true, cmdKey: '11' }
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: 'Wrk Plant Equipment',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            first: 0,
            columns: [
				{ field: 'centreCodeKey', header: 'CN', isReadOnly: true, isSearch: true },
				{ field: 'awhBuisnessSegment', header: 'Sg', isReadOnly: true, isSearch: true },
				{ field: 'equipmentTypeCode', header: 'Type', isReadOnly: true, isSearch: true },
				{ field: 'equipmentLeasor', header: 'Leasor', isReadOnly: true, isSearch: true },
				{ field: 'plantEquipmentCode', header: 'Code', isReadOnly: true, isSearch: true },
				{ field: 'equipmentStatus', header: 'St', isReadOnly: true },
				{ field: 'readingFrequency', header: 'F', isReadOnly: true },
				{ field: 'equipmentDescription', header: 'Description', isReadOnly: true },
				// { field: 'dateDdmm', header: 'Date DDMM', isReadOnly: true },
				{ field: 'lastReading', header: 'Last Reading', isReadOnly: true },
				{ field: 'readingType', header: 'Typ', isReadOnly: true, isSearch: true },
				{ field: 'readingValue', header: 'Service', isReadOnly: true },
				{ field: 'visualEquipmentCode', header: 'Vis Code', isReadOnly: true },
            ]
        };
    }

    ngOnInit() {
        this.plantEquipment = this.client.getModel();
        this.dialogData.flatVariable = this.plantEquipment;
        this.dialogData.rowsDisplayed = this.plantEquipment['size'];
        if (this.plantEquipment['pageGdo']) {
            this.dialogData.data = this.plantEquipment['pageGdo'].content;
            this.dialogData.totalRecords = this.plantEquipment['pageGdo'].totalElements;
            this.dialogData.flatVariable = this.plantEquipment;
        }
    }

    dialogEvent($event) {
        this.mySelections = $event;
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages(
                'info',
                'Please select a record ',
                'Please select a record '
            );
            return;
        }
        this.mySelections.forEach((routeParams, idx) => {
            //routeParams._sysSelected = selected;
            this.mySelections[idx] = routeParams;
            this.mySelections[idx]['_SysRecordSelected'] = 'Y';
            this.mySelections[idx]['_SysSelected'] = selected;
        });
        // this.plantEquipment['pageGdo'] = this.mySelections;
        // if (this.plantEquipment['pageGdo'].length > 0) {
        //   this.plantEquipment['reloadSubfile'] = 'Y';
        // }
        this.onSubmit();
    }

    onSubmit() {
        this.plantEquipment['_SysCmdKey'] = '00';
        //this.plantEquipment.pageGdo = new PageGdoObject();
        if (this.plantEquipment['pageGdo']) {
            this.plantEquipment['pageGdo'].content = this.mySelections;
        }
        this.client.reply();
    }
}
