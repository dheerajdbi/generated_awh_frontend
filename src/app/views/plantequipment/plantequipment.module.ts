
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { Routes, RouterModule } from '@angular/router';

import { WrkPlantEquipmentComponent} from './wrkplantequipment/wrkPlantEquipment.component';
import { EdtPlantEquipmentEntryPanelComponent} from './edtplantequipment/edtPlantEquipmentEntryPanel.component';
import { EdtPlantEquipmentPanelComponent} from './edtplantequipment/edtPlantEquipmentPanel.component';
import { AddPlantEquipmentComponent} from './addplantequipment/addPlantEquipment.component';
import { EquipmentReadingsWspltwmdfkComponent} from './equipmentreadingswspltwmdfk/equipmentReadingsWspltwmdfk.component';
import { BuildReadingBatchComponent} from './buildreadingbatch/buildReadingBatch.component';
import { DisplayPlantEquipmentEntryPanelComponent} from './displayplantequipment/displayPlantEquipmentEntryPanel.component';
import { DisplayPlantEquipmentPanelComponent} from './displayplantequipment/displayPlantEquipmentPanel.component';
import { RenamemPlantEquipmentComponent} from './renamemplantequipment/renamemPlantEquipment.component'; 
 
export const ROUTES: Routes = [
    {
        path:'wrkPlantEquipment',
        component: WrkPlantEquipmentComponent
    },
    {
        path:'edtPlantEquipmentEntryPanel',
        component: EdtPlantEquipmentEntryPanelComponent
    },
    
    {
        path:'edtPlantEquipmentPanel',
        component: EdtPlantEquipmentPanelComponent
    },
    
    {
        path:'addPlantEquipment',
        component: AddPlantEquipmentComponent
    },
    {
        path:'equipmentReadingsWspltwmdfk',
        component: EquipmentReadingsWspltwmdfkComponent
    },
    {
        path:'buildReadingBatch',
        component: BuildReadingBatchComponent
    },
    {
        path:'displayPlantEquipmentEntryPanel',
        component: DisplayPlantEquipmentEntryPanelComponent
    },
    
    {
        path:'displayPlantEquipmentPanel',
        component: DisplayPlantEquipmentPanelComponent
    },
    
    {
        path:'renamemPlantEquipment',
        component: RenamemPlantEquipmentComponent
    },
];

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(ROUTES)
	],

	declarations: [
		WrkPlantEquipmentComponent,
		EdtPlantEquipmentEntryPanelComponent,
		EdtPlantEquipmentPanelComponent,
		AddPlantEquipmentComponent,
		EquipmentReadingsWspltwmdfkComponent,
		BuildReadingBatchComponent,
		DisplayPlantEquipmentEntryPanelComponent,
		DisplayPlantEquipmentPanelComponent,
		RenamemPlantEquipmentComponent,
	],

	exports: [
		WrkPlantEquipmentComponent,
		EdtPlantEquipmentEntryPanelComponent,
		EdtPlantEquipmentPanelComponent,
		AddPlantEquipmentComponent,
		EquipmentReadingsWspltwmdfkComponent,
		BuildReadingBatchComponent,
		DisplayPlantEquipmentEntryPanelComponent,
		DisplayPlantEquipmentPanelComponent,
		RenamemPlantEquipmentComponent,
	]
})

export class PlantEquipmentModule {}