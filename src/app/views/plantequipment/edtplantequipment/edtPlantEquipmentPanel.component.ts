import { Component, OnInit } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
    
@Component({

    selector: 'app-edtPlantEquipmentPanel',
    templateUrl: './edtPlantEquipmentPanel.component.html'
})
    
export class EdtPlantEquipmentPanelComponent implements OnInit {
    plantEquipment = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    equipReadingUnit = [
		{ value: '_hrs', code: 'hrs', description: 'hrs - Hours' },
		{ value: '_km', code: 'km', description: 'km - Kilometres' },
		{ value: '_tn', code: 'tn', description: 'tn - Tonnes' },
		{ value: '_kg', code: 'kg', description: 'kg - Kilograms' },
		{ value: '_kWh', code: 'kWh', description: 'kWh - Kilowatt-hour' },
		{ value: '_MJ', code: 'MJ', description: 'MJ - Megajoules' },
	];
	awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	];
	readingFrequency = [
		{ value: '_W', code: 'W', description: 'W - Weekly' },
		{ value: '_M', code: 'M', description: 'M - Monthly' },
		{ value: '_F', code: 'F', description: 'F - Fortnightly' },
	];

    constructor(private client: TerminalClientService) {
        this.plantEquipment = client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];

    }

    ngOnInit() {
        this.plantEquipment = this.client.getModel();
    }

    onSubmit() {
    	this.plantEquipment['_SysCmdKey'] = '00';
        this.client.reply();
    }
}