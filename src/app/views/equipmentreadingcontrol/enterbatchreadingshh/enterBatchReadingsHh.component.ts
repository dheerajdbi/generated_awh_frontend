
import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-enterBatchReadingsHh',
    templateUrl: './enterBatchReadingsHh.component.html'
})
    
export class EnterBatchReadingsHhComponent implements OnInit {
    public equipmentReadingControl= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService) {
        this.equipmentReadingControl = client.getModel();
        
    }

    ngOnInit() {
    }

    onSubmit() {
        this.equipmentReadingControl['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
