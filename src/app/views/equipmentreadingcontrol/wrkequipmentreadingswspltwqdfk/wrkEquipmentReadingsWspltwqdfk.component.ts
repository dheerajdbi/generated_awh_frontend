import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-wrkEquipmentReadingsWspltwqdfk',
    templateUrl: './wrkEquipmentReadingsWspltwqdfk.component.html'
})
    
export class WrkEquipmentReadingsWspltwqdfkComponent implements OnInit {
    equipmentReadingControl: any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    equipReadingCtrlSts = [
		{ value: '_C', code: 'C', description: 'C - Created' },
		{ value: '_I', code: 'I', description: 'I - In-Progress' },
		{ value: '_X', code: 'X', description: 'X - Complete' },
	]

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' },
			{ id: 'fKey-11', btnTitle: 'Show', signal: 'funKey11', display: true, cmdKey: '11' }
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: 'Wrk Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
				{ field: 'centreCodeKey', header: 'Centre Code           KEY', isReadOnly: true, isSearch: true },
				{ field: 'centreNameDrv', header: 'Centre Name Drv', isReadOnly: true },
				{ field: 'readingRequiredDate', header: 'Reading Required Date', isReadOnly: true, isSearch: true },
				{ field: 'equipReadingBatchNo', header: 'Equip Reading Batch No.', isReadOnly: true },
				{ field: 'equipReadingCtrlSts', header: 'Equip Reading Ctrl Sts', isReadOnly: true, isSearch: true },
				{ field: 'conditionName', header: '*Condition name', isReadOnly: true },
            ]
        };
    }

    ngOnInit() {
        this.equipmentReadingControl = this.client.getModel();
        this.dialogData.data = this.equipmentReadingControl.pageGdo.content;
    }

    dialogEvent($event) {
        this.mySelections = $event;
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages('info', 'Please select a record ', 'Please select a record ');
            return;
        }
        this.mySelections.forEach((routeParams, idx) => {
            routeParams._SysSelected = selected;
            this.mySelections[idx] = routeParams;
        });
        this.onSubmit()
    }
    
    onSubmit() {
        this.equipmentReadingControl['_SysCmdKey'] = '00';
        this.equipmentReadingControl.pageGdo = new PageGdoObject();
        this.equipmentReadingControl.pageGdo.content = this.mySelections;
        this.client.reply();
    }
}
