
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { Routes, RouterModule } from '@angular/router';

import { WrkEquipmentReadingsWspltwqdfkComponent} from './wrkequipmentreadingswspltwqdfk/wrkEquipmentReadingsWspltwqdfk.component';
import { EnterBatchReadingsHhComponent} from './enterbatchreadingshh/enterBatchReadingsHh.component'; 
 
export const ROUTES: Routes = [
    {
        path:'wrkEquipmentReadingsWspltwqdfk',
        component: WrkEquipmentReadingsWspltwqdfkComponent
    },
    {
        path:'enterBatchReadingsHh',
        component: EnterBatchReadingsHhComponent
    },
];

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(ROUTES)
	],

	declarations: [
		WrkEquipmentReadingsWspltwqdfkComponent,
		EnterBatchReadingsHhComponent,
	],

	exports: [
		WrkEquipmentReadingsWspltwqdfkComponent,
		EnterBatchReadingsHhComponent,
	]
})

export class EquipmentReadingControlModule {}