import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TerminalClientState } from '../../../framework/services/terminal-client.service';
import { BehaviorSubject } from 'rxjs/Rx';
import { DspEquipmentReadingEntryPanelComponent } from './dspEquipmentReadingEntryPanel.component';

describe('Component: DspEquipmentReadingEntryPanelComponent', () => {
    let component: DspEquipmentReadingEntryPanelComponent;
    let fixture: ComponentFixture<DspEquipmentReadingEntryPanelComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [DspEquipmentReadingEntryPanelComponent],
            providers: [
                Logger,
                {
                    provide: TerminalClientService,
                    useValue: {
                        reply: jasmine.createSpy(null, null),
                        setSignal: jasmine.createSpy(null, null),
                        getModel: jasmine.createSpy(null, null).and.returnValue({
                        }),
                        state: new BehaviorSubject(TerminalClientState.INITIALIZING)
                    }
                },
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DspEquipmentReadingEntryPanelComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be named `DspEquipmentReadingEntryPanelComponent`', () => {
        expect(DspEquipmentReadingEntryPanelComponent.name).toBe('DspEquipmentReadingEntryPanelComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(DspEquipmentReadingEntryPanelComponent.prototype.onSubmit).not.toBeNull();
    });
});