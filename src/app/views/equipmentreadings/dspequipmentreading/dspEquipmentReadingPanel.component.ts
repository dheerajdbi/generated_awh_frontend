import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { AppService } from '../../../app.service';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
    
@Component({

    selector: 'app-dspEquipmentReadingPanel',
    templateUrl: './dspEquipmentReadingPanel.component.html'
})
    
export class DspEquipmentReadingPanelComponent implements OnInit {
    equipmentReadings: any;
    mode = 'init';
	public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService) {
        this.equipmentReadings = client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' },
			{ id: 'fKey-24', btnTitle: 'Key', signal: 'funKey24', display: true, cmdKey: '24' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }

    ngOnInit() {

    }

    onSubmit() {
        this.client.reply();
    }
}