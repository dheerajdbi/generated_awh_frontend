import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-wrkEquipmentReadingsWspltwsdfk',
    templateUrl: './wrkEquipmentReadingsWspltwsdfk.component.html'
})
    
export class WrkEquipmentReadingsWspltwsdfkComponent implements OnInit {
    equipmentReadings: any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'Add', signal: 'funKey06', display: true, cmdKey: '06' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' }
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: 'Wrk Equipment Readings',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
				{ field: 'readingRequiredDate', header: 'Reading Required Date', isReadOnly: true },
				{ field: 'readingType', header: 'Reading Type', isReadOnly: true },
				{ field: 'conditionName', header: '*Condition name', isReadOnly: true },
				{ field: 'readingValue', header: 'Reading Value', isReadOnly: true },
				{ field: 'centreCodeKey', header: 'Centre Code           KEY', isReadOnly: true },
				{ field: 'awhBuisnessSegment', header: 'AWH Buisness Segment', isReadOnly: true },
				{ field: 'transferToBuisSeg', header: 'Transfer to Buis Seg', isReadOnly: true },
				{ field: 'transferToCentre', header: 'Transfer to Centre', isReadOnly: true },
				{ field: 'differenceFromPrevious', header: 'Difference from Previous', isReadOnly: true },
				{ field: 'idleDaysFromPrevious', header: 'Idle Days from Previous', isReadOnly: true },
				{ field: 'unavailableDaysFromPrv', header: 'Unavailable Days from Prv', isReadOnly: true },
				{ field: 'readingComment', header: 'Reading Comment', isReadOnly: true },
            ]
        };
    }

    ngOnInit() {
        this.equipmentReadings = this.client.getModel();
        this.dialogData.data = this.equipmentReadings.pageGdo.content;
    }

    dialogEvent($event) {
        this.mySelections = $event;
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages('info', 'Please select a record ', 'Please select a record ');
            return;
        }
        this.mySelections.forEach((routeParams, idx) => {
            routeParams._SysSelected = selected;
            this.mySelections[idx] = routeParams;
        });
        this.onSubmit()
    }
    
    onSubmit() {
        this.equipmentReadings['_SysCmdKey'] = '00';
        this.equipmentReadings.pageGdo = new PageGdoObject();
        this.equipmentReadings.pageGdo.content = this.mySelections;
        this.client.reply();
    }
}
