import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AddmEquipmentReadingsComponent } from './addmEquipmentReadings.component';

describe('Component: AddmEquipmentReadingsComponent', () => {
    let component: AddmEquipmentReadingsComponent;
    let fixture: ComponentFixture<AddmEquipmentReadingsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [AddmEquipmentReadingsComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddmEquipmentReadingsComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be named `AddmEquipmentReadingsComponent`', () => {
        expect(AddmEquipmentReadingsComponent.name).toBe('AddmEquipmentReadingsComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(AddmEquipmentReadingsComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(AddmEquipmentReadingsComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(AddmEquipmentReadingsComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(AddmEquipmentReadingsComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(AddmEquipmentReadingsComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(AddmEquipmentReadingsComponent.prototype.onSubmit).not.toBeNull();
    });
});