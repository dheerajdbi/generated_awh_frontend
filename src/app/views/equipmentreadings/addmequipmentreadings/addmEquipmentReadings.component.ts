
import { Component, OnInit } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
    
@Component({
    selector: 'app-addmEquipmentReadings',
    templateUrl: './addmEquipmentReadings.component.html'
})
    
export class AddmEquipmentReadingsComponent implements OnInit {
    public equipmentReadings= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService) {
        this.equipmentReadings = client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }

    ngOnInit() {
    }

    onSubmit() {
        this.equipmentReadings['_SysCmdKey'] = '00';
        this.client.reply();
    }
}
