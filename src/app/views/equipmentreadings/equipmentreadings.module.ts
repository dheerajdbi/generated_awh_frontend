import { SelectOrganisationDumpComponent } from "./../organisationdump/selectorganisationdump/selectOrganisationDump.component";

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { WrkEquipmentReadingsWspltwsdfkComponent } from "./wrkequipmentreadingswspltwsdfk/wrkEquipmentReadingsWspltwsdfk.component";
import { DspEquipmentReadingEntryPanelComponent } from "./dspequipmentreading/dspEquipmentReadingEntryPanel.component";
import { DspEquipmentReadingPanelComponent } from "./dspequipmentreading/dspEquipmentReadingPanel.component";
import { AddmEquipmentReadingsComponent } from "./addmequipmentreadings/addmEquipmentReadings.component";
import { EdtEquipmentReadingsEntryPanelComponent } from "./edtequipmentreadings/edtEquipmentReadingsEntryPanel.component";
import { EdtEquipmentReadingsPanelComponent } from "./edtequipmentreadings/edtEquipmentReadingsPanel.component";
import { PrtLeasorReadingsPmtComponent } from "./prtleasorreadingspmt/prtLeasorReadingsPmt.component";
import { AddmBatchReadingHhComponent } from "./addmbatchreadinghh/addmBatchReadingHh.component";

export const ROUTES: Routes = [
  {
    path: "wrkEquipmentReadingsWspltwsdfk",
    component: WrkEquipmentReadingsWspltwsdfkComponent
  },
  {
    path: "dspEquipmentReadingEntryPanel",
    component: DspEquipmentReadingEntryPanelComponent
  },

  {
    path: "dspEquipmentReadingPanel",
    component: DspEquipmentReadingPanelComponent
  },

  {
    path: "addmEquipmentReadings",
    component: AddmEquipmentReadingsComponent
  },
  {
    path: "edtEquipmentReadingsEntryPanel",
    component: EdtEquipmentReadingsEntryPanelComponent
  },

  {
    path: "edtEquipmentReadingsPanel",
    component: EdtEquipmentReadingsPanelComponent
  },

  {
    path: "prtLeasorReadingsPmt",
    component: PrtLeasorReadingsPmtComponent
  },
  {
    path: "addmBatchReadingHh",
    component: AddmBatchReadingHhComponent
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(ROUTES)],

  declarations: [
    WrkEquipmentReadingsWspltwsdfkComponent,
    DspEquipmentReadingEntryPanelComponent,
    DspEquipmentReadingPanelComponent,
    AddmEquipmentReadingsComponent,
    EdtEquipmentReadingsEntryPanelComponent,
    EdtEquipmentReadingsPanelComponent,
    PrtLeasorReadingsPmtComponent,
    AddmBatchReadingHhComponent,
    SelectOrganisationDumpComponent
  ],

  exports: [
    WrkEquipmentReadingsWspltwsdfkComponent,
    DspEquipmentReadingEntryPanelComponent,
    DspEquipmentReadingPanelComponent,
    AddmEquipmentReadingsComponent,
    EdtEquipmentReadingsEntryPanelComponent,
    EdtEquipmentReadingsPanelComponent,
    PrtLeasorReadingsPmtComponent,
    AddmBatchReadingHhComponent
  ]
})
export class EquipmentReadingsModule {}
