import { Component, OnInit } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
    
@Component({

    selector: 'app-edtEquipmentReadingsPanel',
    templateUrl: './edtEquipmentReadingsPanel.component.html'
})
    
export class EdtEquipmentReadingsPanelComponent implements OnInit {
    equipmentReadings = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    awhBuisnessSegment = [
		{ value: '_01', code: '01', description: '01 - Wool Handling' },
		{ value: '_02', code: '02', description: '02 - Dumping' },
		{ value: '_03', code: '03', description: '03 - Cotton' },
		{ value: '_04', code: '04', description: '04 - Logistics' },
		{ value: '_05', code: '05', description: '05 - Property' },
		{ value: '_06', code: '06', description: '06 - Dry Bulk' },
		{ value: '_09', code: '09', description: '09 - Re-Handle' },
	];
	readingType = [
		{ value: '_ST', code: 'ST', description: 'ST - Standard Reading' },
		{ value: '_SV', code: 'SV', description: 'SV - Service Reading' },
		{ value: '_TO', code: 'TO', description: 'TO - Transfer Out' },
		{ value: '_TI', code: 'TI', description: 'TI - Transfer In Reading' },
		{ value: '_IL', code: 'IL', description: 'IL - Initial reading' },
		{ value: '_RT', code: 'RT', description: 'RT - Retirement' },
		{ value: '_NE', code: 'NE', description: 'NE - Any' },
		{ value: '_MR', code: 'MR', description: 'MR - Meter Reset' },
		{ value: '_UA', code: 'UA', description: 'UA - Make UnAvailable' },
		{ value: '_AV', code: 'AV', description: 'AV - Make Available' },
	];
	equipReadingUnit = [
		{ value: '_hrs', code: 'hrs', description: 'hrs - Hours' },
		{ value: '_km', code: 'km', description: 'km - Kilometres' },
		{ value: '_tn', code: 'tn', description: 'tn - Tonnes' },
		{ value: '_kg', code: 'kg', description: 'kg - Kilograms' },
		{ value: '_kWh', code: 'kWh', description: 'kWh - Kilowatt-hour' },
		{ value: '_MJ', code: 'MJ', description: 'MJ - Megajoules' },
	];

    constructor(private client: TerminalClientService) {
        this.equipmentReadings = client.getModel();
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-12', btnTitle: 'Cancel', signal: 'funKey12', display: true, cmdKey: '12' },
			{ id: 'fKey-24', btnTitle: 'Key', signal: 'funKey24', display: true, cmdKey: '24' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];

    }

    ngOnInit() {
        this.equipmentReadings = this.client.getModel();
    }

    onSubmit() {
    	this.equipmentReadings['_SysCmdKey'] = '00';
        this.client.reply();
    }
}