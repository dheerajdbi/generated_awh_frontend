import { GridPanelComponent } from '../../../framework/grid-panel/grid-panel.component';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { Component, OnInit , ViewChild } from '@angular/core';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from '../../../framework/services/messages.service';
import { ActivatedRoute } from '@angular/router';
import { deepEqual } from 'assert';
import { PageGdoObject } from '../../../app.component';
    
@Component({
    selector: 'app-editAwhRegion',
    templateUrl: './editAwhRegion.component.html'
})
    
export class EditAwhRegionComponent implements OnInit {
    awhRegion: any;
    public mySelections: any[] = [];
    public loader = false;
    public mode: string;
    consts: {};
    gdo: any;
    public flatVariable: any;
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: 'fKey-03', btnTitle: 'Exit', signal: 'funKey03', display: true, cmdKey: '03' },
			{ id: 'fKey-06', btnTitle: 'Go', signal: 'funKey06', display: true, cmdKey: '06' }
		];
		this.searchParams = [
			{ id: 'fKey-04', btnTitle: 'Prompt', signal: 'funKey04', display: true, cmdKey: '04'}
		];
    }
 
	dialogEvent($event) {
        this.mySelections = $event;
        this.mySelections.forEach((routeParams, idx) => {
            routeParams['_SysSelected'] = this.awhRegion._SysProgramMode === 'CHG' ? 'CHANGE' : 'ADD';
            this.mySelections[idx] = routeParams;
        });
    }

	ngOnInit() {
        this.awhRegion = this.client.getModel();
        this.dialogData = {
            header: 'Edit AWH Region',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: this.awhRegion.size,
            
            tablePaginator: true,
            flatVariable: this.awhRegion,
            totalRecords: this.awhRegion.pageGdo.totalElements,
            data: this.awhRegion.pageGdo.content,
            
            columns: [
				{ field: 'awhRegionCode', header: 'AWH Region Code', isFilter: true, editable: true },
				{ field: 'awhRegionName', header: 'AWH Region Name', isFilter: true, editable: true },
				{ field: 'awhRegionOrg', header: 'AWH Region Org', isFilter: true, editable: true },
				{ field: 'awhRegionContact', header: 'AWH Region Contact', isFilter: true, editable: true },
				{ field: 'contactName', header: 'Contact Name', editable: true, isReadOnly: true },
            ]
        };
        if (this.awhRegion['_SysProgramMode'] == 'ADD') {
            this.funcParams[1].btnTitle = 'Change';
        }
        else if (this.awhRegion['_SysProgramMode'] == 'CHG') {
            this.funcParams[1].btnTitle = 'Add';
        }
    }

    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages(
                'info',
                'Please select a record ',
                'Please select a record '
             );
            return;
        }

        this.mySelections.forEach((routeParams, idx) => {
            routeParams['_SysSelected'] = selected;
            this.mySelections[idx] = routeParams;
        });

        if (selected === '*Delete#1'){
            this.awhRegion.pageGdo = new PageGdoObject();
            this.awhRegion.pageGdo["content"] = this.mySelections;
            this.client.confirm();
        } else {
            this.onSubmit();
        }

        this.awhRegion.pageGdo = new PageGdoObject();
        this.awhRegion.pageGdo["content"] = this.mySelections;
        this.awhRegion["_SysCmdKey"] = "00";
        this.client.confirm();
    }

    onSubmit() {
        this.awhRegion["_SysCmdKey"] = "00";
        this.awhRegion.pageGdo = new PageGdoObject();
        this.awhRegion.pageGdo["content"] = this.mySelections;
        this.client.reply();
    }
}
