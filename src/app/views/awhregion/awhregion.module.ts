import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectAwhRegionComponent } from "./selectawhregion/selectAwhRegion.component";
import { EditAwhRegionComponent } from "./editawhregion/editAwhRegion.component";

export const ROUTES: Routes = [
  {
    path: "selectAwhRegion",
    component: SelectAwhRegionComponent
  },
  {
    path: "editAwhRegion",
    component: EditAwhRegionComponent
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(ROUTES)],

  declarations: [SelectAwhRegionComponent, EditAwhRegionComponent],

  exports: [SelectAwhRegionComponent, EditAwhRegionComponent]
})
export class AwhRegionModule {}
