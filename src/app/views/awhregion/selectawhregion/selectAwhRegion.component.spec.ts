import { SharedModule } from '../../../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentFixture } from '@angular/core/testing';
import { TestBed, async } from '@angular/core/testing';
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SelectAwhRegionComponent} from './selectAwhRegion.component';

describe('Component: SelectAwhRegionComponent', () => {
    let component: SelectAwhRegionComponent;
    let fixture: ComponentFixture<SelectAwhRegionComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                CommonModule,
                FormsModule,
                GrowlModule,
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
                SharedModule
            ],
            declarations: [SelectAwhRegionComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: '/'
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(SelectAwhRegionComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `SelectAwhRegionComponent`',() => {
        expect(SelectAwhRegionComponent.name).toBe('SelectAwhRegionComponent');
    });

    it('should have a method called `constructor`', () => {
        expect(SelectAwhRegionComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(SelectAwhRegionComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(SelectAwhRegionComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(SelectAwhRegionComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', () => {
        expect(SelectAwhRegionComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', () => {
        expect(SelectAwhRegionComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', () => {
        expect(SelectAwhRegionComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(SelectAwhRegionComponent.prototype.onSubmit).toBeDefined();
    });
});