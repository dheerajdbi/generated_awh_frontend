import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import {
  TerminalClientService,
  TerminalClientState
} from "../../../framework/services/terminal-client.service";
import { PageGdoObject } from "../../../app.component";
import { Table } from "primeng/table";

@Component({
  selector: "app-selectAwhRegion",
  templateUrl: "./selectAwhRegion.component.html"
})
export class SelectAwhRegionComponent implements OnInit {
  public awhRegion: any;
  public funcParams: FunctionKey[];
  gridVariable: any;
  totalRecords;
  displayNoSelection = false;
  mySelections = [];
  visible = true;
  cols = [];
  init: boolean = false;
  onSort: boolean = false;
  @ViewChild(Table) turboTable: Table;
  @Input() display = false;
  constructor(private client: TerminalClientService) {
    this.funcParams = [
      { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
      { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
    ];

    this.cols = [
      {
        field: "awhRegionCode",
        header: "AWH Region Code",
        editable: true,
        isReadOnly: true
      },
      {
        field: "awhRegionName",
        header: "AWH Region Name",
        editable: true,
        isReadOnly: true
      }
    ];

    this.awhRegion = client.getModel();
  }

  ngOnInit() {
    if (this.awhRegion.pageGdo) {
      this.gridVariable = this.awhRegion.pageGdo.content;
      this.totalRecords = this.awhRegion.pageGdo.totalElements;
    }
  }

  processGrid(event) {
    if (this.init) {
      if (this.onSort) {
        this.customSort(event);
        this.turboTable.first = this.awhRegion["page"] * this.awhRegion["size"];
        this.onSort = false;
        return;
      }
      this.awhRegion["page"] = event.first / event.rows;
      this.awhRegion["size"] = event.rows;
      this.awhRegion["gdo"] = null;
      this.awhRegion["_SysCmdKey"] = "08";
      this.awhRegion.pageGdo = new PageGdoObject();
      this.onSubmit();
    } else {
      this.init = true;
      this.turboTable.first = this.awhRegion["page"] * this.awhRegion["size"];
    }
  }

  customSort(event) {
    this.awhRegion.pageGdo.content.sort((t1, t2) => {
      if (event.sortOrder === 1) {
        if (t1[event.sortField] > t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] < t2[event.sortField]) {
          return -1;
        }
      } else {
        if (t1[event.sortField] < t2[event.sortField]) {
          return 1;
        }
        if (t1[event.sortField] > t2[event.sortField]) {
          return -1;
        }
      }
      return 0;
    });
  }

  onSubmit() {
    this.awhRegion["_SysCmdKey"] = "00";
    if (this.mySelections) {
      this.awhRegion["_SysEntrySelected"] = "Y";
      this.awhRegion.awhRegionCode = this.mySelections["awhRegionCode"];
    } else {
      this.awhRegion["_SysEntrySelected"] = "";
    }
    this.client.reply();
  }

  ngDoCheck() {
    if (this.client.$display) {
      this.awhRegion = this.client.getModel();
      this.gridVariable = this.awhRegion.pageGdo.content;
      this.totalRecords = this.awhRegion.pageGdo.totalElements;
    }
  }
  ngOnChange() {
    this.gridVariable = this.awhRegion.pageGdo.content;
    this.totalRecords = this.awhRegion.pageGdo.totalElements;
  }
}
