
import { Component, OnInit, ViewChild  } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
import { PageGdoObject } from '../../../app.component';
import { Table } from 'primeng/table';


@Component({
    selector: 'app-selectOrganisationConta',
    templateUrl: './selectOrganisationConta.component.html'
})

export class SelectOrganisationContaComponent implements OnInit {
    public organisationContact: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;
    onSort: boolean = false;
    @ViewChild(Table) turboTable: Table;
 
    constructor(private client: TerminalClientService) {
        this.funcParams = [
            { id: 'fKey-01', btnTitle: 'Ok', signal: 'funKey01', display: true },
            { id: 'fKey-03', btnTitle: 'Cancel', signal: 'funKey03', display: true }
        ];

        this.cols = [
			{ field: 'organisation', header: 'Organisation', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactSequence', header: 'Org Contact Sequence', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactName', header: 'Org Contact Name', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactJobTitle', header: 'Org Contact Job Title', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactStatus', header: 'Org Contact Status', editable: true, isDropDown: true, isReadOnly: true, isSearch: true },
			{ field: 'orgContactIsDefault', header: 'Org Contact is Default', editable: true, isDropDown: true, isReadOnly: true, isSearch: true },
        ];

        this.organisationContact = client.getModel();
    }

    ngOnInit() {
    	if (this.organisationContact.pageGdo) {
    		this.gridVariable = this.organisationContact.pageGdo.content;
    		this.totalRecords = this.organisationContact.pageGdo.totalElements;
    	}
    }

    processGrid(event) {
        if (this.init) {
            if (this.onSort) {
                this.customSort(event);
                this.turboTable.first = this.organisationContact['page'] * this.organisationContact['size'];
                this.onSort = false;
                return;
            }
            this.organisationContact['page'] = event.first / event.rows;
            this.organisationContact['size'] = event.rows;
            this.organisationContact['gdo'] = null;
            this.organisationContact['_SysCmdKey'] = '08';
            this.organisationContact.pageGdo = new PageGdoObject();
            this.onSubmit(3);
        } else {
            this.init = true;
            this.turboTable.first = this.organisationContact['page'] * this.organisationContact['size'];
        }
    }
 
    customSort(event) {
        this.organisationContact.pageGdo.content.sort((t1, t2) => {
            if (event.sortOrder === 1) {
                if (t1[event.sortField] > t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] < t2[event.sortField]) {
                    return -1;
                }
            } else {
                if (t1[event.sortField] < t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] > t2[event.sortField]) {
                    return -1;
                }
            }
            return 0;
        });
    }

    onSubmit(val) {
        if (val === 1) {
            if (this.mySelections !== undefined && this.mySelections !== null && this.mySelections) {
//                this.organisationContact['gdo'] = this.mySelections;
				this.organisationContact['organisation'] = this.mySelections['organisation'];
				this.organisationContact['orgContactSequence'] = this.mySelections['orgContactSequence'];
            }
            else {
                this.displayNoSelection = true;
            }
        }
        else if (val === 2) {
            this.organisationContact['gdo'] = null;
            this.organisationContact['_SysCmdKey'] = '03';
    	}
        this.organisationContact.pageGdo = new PageGdoObject();
        this.client.reply();
    }
}
