
import { Component, OnInit, ViewChild  } from '@angular/core';
import { FunctionKey } from '../../../framework/function-key-panel/function-key-panel.component';
import { TerminalClientService, TerminalClientState } from '../../../framework/services/terminal-client.service';
import { PageGdoObject } from '../../../app.component';
import { Table } from 'primeng/table';


@Component({
    selector: 'app-wsut5502SelectCentre',
    templateUrl: './wsut5502SelectCentre.component.html'
})

export class Wsut5502SelectCentreComponent implements OnInit {
    public centre: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;
    onSort: boolean = false;
    @ViewChild(Table) turboTable: Table;
 
    constructor(private client: TerminalClientService) {
        this.funcParams = [
            { id: 'fKey-01', btnTitle: 'Ok', signal: 'funKey01', display: true },
            { id: 'fKey-03', btnTitle: 'Cancel', signal: 'funKey03', display: true }
        ];

        this.cols = [
			{ field: 'centreCodeKey', header: 'Centre Code           KEY', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'centreName', header: 'Centre Name', editable: true, isReadOnly: true, isSearch: true },
			{ field: 'centreType', header: 'Centre Type', editable: true, isDropDown: true, isReadOnly: true },
			{ field: 'stateCode', header: 'State Code', editable: true, isDropDown: true, isReadOnly: true },
			{ field: 'centreRegion', header: 'Centre Region', editable: true, isReadOnly: true },
        ];

        this.centre = client.getModel();
    }

    ngOnInit() {
    	if (this.centre.pageGdo) {
    		this.gridVariable = this.centre.pageGdo.content;
    		this.totalRecords = this.centre.pageGdo.totalElements;
    	}
    }

    processGrid(event) {
        if (this.init) {
            if (this.onSort) {
                this.customSort(event);
                this.turboTable.first = this.centre['page'] * this.centre['size'];
                this.onSort = false;
                return;
            }
            this.centre['page'] = event.first / event.rows;
            this.centre['size'] = event.rows;
            this.centre['gdo'] = null;
            this.centre['_SysCmdKey'] = '08';
            this.centre.pageGdo = new PageGdoObject();
            this.onSubmit(3);
        } else {
            this.init = true;
            this.turboTable.first = this.centre['page'] * this.centre['size'];
        }
    }
 
    customSort(event) {
        this.centre.pageGdo.content.sort((t1, t2) => {
            if (event.sortOrder === 1) {
                if (t1[event.sortField] > t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] < t2[event.sortField]) {
                    return -1;
                }
            } else {
                if (t1[event.sortField] < t2[event.sortField]) {
                    return 1;
                }
                if (t1[event.sortField] > t2[event.sortField]) {
                    return -1;
                }
            }
            return 0;
        });
    }

    onSubmit(val) {
        if (val === 1) {
            if (this.mySelections !== undefined && this.mySelections !== null && this.mySelections) {
//                this.centre['gdo'] = this.mySelections;
				this.centre['centreCodeKey'] = this.mySelections['centreCodeKey'];
            }
            else {
                this.displayNoSelection = true;
            }
        }
        else if (val === 2) {
            this.centre['gdo'] = null;
            this.centre['_SysCmdKey'] = '03';
    	}
        this.centre.pageGdo = new PageGdoObject();
        this.client.reply();
    }
}
