import { MainMenuComponent } from "./framework/main-menu/main-menu.component";
import { AppRoutesModule } from "./app.routes";
import { SharedModule } from "./shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { WaitComponent } from "./framework/wait/wait.component";
import { LoginComponent } from "./framework/login/login.component";
import { AppService } from "./app.service";
import { AwhRegionModule } from "./views/awhregion/awhregion.module";
import { PlantEquipmentModule } from "./views/plantequipment/plantequipment.module";
import { EquipmentReadingsModule } from "./views/equipmentreadings/equipmentreadings.module";
import { EquipmentTypeModule } from "./views/equipmenttype/equipmenttype.module";
import { EquipmentReadingControlModule } from "./views/equipmentreadingcontrol/equipmentreadingcontrol.module";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WaitComponent,
    MainMenuComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AwhRegionModule,
    PlantEquipmentModule,
    EquipmentReadingsModule,
    EquipmentTypeModule,
    EquipmentReadingControlModule,
    AppRoutesModule
  ],
  providers: [Location, AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}
